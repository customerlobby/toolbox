require 'spec_helper'
RSpec.describe Toolbox::Client::Transactions do
  before do
    @client = Toolbox::Client.new
  end

  def call_get_transactions_with_response(response)
    tb = Toolbox::Client.new(api_key: 'FAKE-ACCESS-KEY')

    params = {
      start_date: Date.new(2014, 9, 1),
      end_date:   Date.new(2014, 9, 30),
      zor_num:    2,
      fran_num:   9998
    }

    expect(tb).to receive(:get).with('GetCustomerListByFranNum', params).and_return(response)
    tb.transactions(params)
  end

end
