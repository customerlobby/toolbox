require 'spec_helper'
require 'byebug'

RSpec.describe Toolbox::Client::CustomersTransactions do
  before do
    @client = Toolbox::Client.new
  end

  describe '.customers_transactions' do

    context 'Call unsuccessful' do
      before(:all) do
        response_class = Struct.new(:status, :body)
        @response = response_class.new(500,"There was an error processing the request.")
      end

      it 'Should raise an exception' do
        expect{ call_get_customers_and_transactions_with_response(@response) }.to raise_exception(Exception)
      end

    end

    context 'Call to api successful' do

      before(:all) do
        response_class = Struct.new(:status, :body)
        @response = response_class.new(200, File.open('spec/fixtures/raw-response.xml','r').read)
      end

      it 'should return an object that responds to :customers and :transactions' do
        results = call_get_customers_and_transactions_with_response(@response)

        expect(results.respond_to?(:customers)).to eq(true)
        expect(results.respond_to?(:transactions)).to eq(true)

        expect(results.customers.class).to eq(Array)
        expect(results.customers.first.class).to eq(Hash)
        expect(results.transactions.class).to eq(Array)
        expect(results.transactions.first.class).to eq(Hash)

      end

    end

  end

  def call_get_customers_and_transactions_with_response(response)
    toolbox = Toolbox::Client.new(api_key: "FAKE-ACCESS-KEY")

    params = {
      start_date: Date.new(2014,9,1),
      end_date: Date.new(2014,9,30),
      zor_num: 2,
      fran_num: 9998
    }

    expect(toolbox).to receive(:get).with("GetCustomerListByFranNum",params).and_return(response.body)
    toolbox.customers_transactions(params)
  end

end
