require 'spec_helper'

RSpec.describe Toolbox::XmlReader do
  let(:xml_string) { File.open("spec/fixtures/xml-reader-test.xml", 'r').read }
  let(:xml_reader) { Toolbox::XmlReader.new(xml_string) }

  context 'contacts' do
    it 'should return the correct count of contacts' do
      expect(xml_reader.customers.count).to eq(91)
    end

    it 'should return the fields correctly for a person' do
      customer = xml_reader.customers.first

      expect(customer[:first_name]).to eq('Mary')
      expect(customer[:last_name]).to eq('Frailich')
      expect(customer[:company_name]).to eq(nil)
      expect(customer[:address1]).to eq('22128 Palais Pl')
      expect(customer[:address2]).to eq(nil)
      expect(customer[:city]).to eq('CALABASAS')
      expect(customer[:state]).to eq('CA')
      expect(customer[:zip]).to eq('91302')
      expect(customer[:country]).to eq(nil)
      expect(customer[:email]).to eq('smflawcorporate@aol.com')
      expect(customer[:phone_daytime]).to eq('8185912916')
      expect(customer[:phone_daytime_ext]).to eq(nil)
      expect(customer[:phone_mobile]).to eq(nil)
      expect(customer[:wants_communication]).to eq('1')
      expect(customer[:commercial]).to eq('0')
    end

    it 'should return the fields correctly for a customer' do
      customer = xml_reader.customers.select{|c| c[:external_id]=='29771'}.first

      expect(customer[:first_name]).to eq('John Devery Wine &amp; Cheese')
      expect(customer[:last_name]).to eq(nil)
      expect(customer[:commercial]).to eq('1')
      expect(customer[:company_name]).to eq('John Devery Wine &amp; Cheese')
    end

    it "should handle names like 'Morgan?,' " do
      customer = xml_reader.customers.select{|c| c[:external_id]=='28101'}.first

      expect(customer[:first_name]).to eq('Morgan?,')
      expect(customer[:last_name]).to eq(nil)
      expect(customer[:company_name]).to eq(nil)
    end
  end

  context 'transactions' do
    it 'should return the correct count of transactions' do
      expect(xml_reader.transactions.count).to eq(8)
    end

    it 'returns the fields for a single job transaction' do
      transaction = xml_reader.transactions.select{|t| t[:external_id]=='33982'}.first

      expect(transaction[:external_id]).to eq('33982')
      expect(transaction[:external_contact_id]).to eq('44292')
      expect(transaction[:receipt_number]).to eq('33982')
      expect(transaction[:due_date]).to eq(DateTime.new(2014,9,4,16,0,0))
      expect(transaction[:transacted_at]).to eq(DateTime.new(2014,9,4,16,0,0))
      expect(transaction[:amount]).to eq(80.0)
      expect(transaction[:is_paid]).to eq('1')
    end

    it 'returns the fields for a single job transaction where status is exported' do
      transaction = xml_reader.transactions.select{|t| t[:external_id]=='437520'}.first

      expect(transaction[:external_id]).to eq('437520')
      expect(transaction[:external_contact_id]).to eq('6234756')
      expect(transaction[:receipt_number]).to eq('437520')
      expect(transaction[:due_date]).to eq(DateTime.new(2016,12,6,13,0,0))
      expect(transaction[:transacted_at]).to eq(DateTime.new(2016,12,6,13,0,0))
      expect(transaction[:amount]).to eq(1249.0300)
      expect(transaction[:is_paid]).to eq('1')
    end

    it 'returns the fields for a single job transaction where field is JobStatus' do
      transaction = xml_reader.transactions.select{|t| t[:external_id]=='13650'}.first

      expect(transaction[:external_id]).to eq('13650')
      expect(transaction[:external_contact_id]).to eq('12345')
      expect(transaction[:receipt_number]).to eq('13650')
      expect(transaction[:due_date]).to eq(DateTime.new(2016,12,6,12,0,0))
      expect(transaction[:transacted_at]).to eq(DateTime.new(2016,12,6,12,0,0))
      expect(transaction[:amount]).to eq(135.0)
      expect(transaction[:is_paid]).to eq('1')
    end

    context 'completed multiple job transactions' do
      let(:external_id) { '34075' }
      let(:external_contact_id) { '44384' }
      let(:transaction) { xml_reader.transactions.select{|t| t[:external_id]==external_id}.first }
      let(:transacted_at) { DateTime.new(2014,9,17,12,30,0) }

      it 'should return the external_id, receipt_number and external_contact_id correctly' do
        expect(transaction[:external_id]).to eq(external_id)
        expect(transaction[:external_contact_id]).to eq(external_contact_id)
        expect(transaction[:receipt_number]).to eq(external_id)
      end

      it 'should return the maximum date of active jobs' do
        expect(transaction[:due_date]).to eq(transacted_at)
        expect(transaction[:transacted_at]).to eq(transacted_at)
      end

      it 'should return the total amount of active jobs' do
        expect(transaction[:amount]).to eq(101.0000)
      end

      it 'should return is_paid as true for complete projects' do
        expect(transaction[:is_paid]).to eq('1')
      end
    end

    context 'incomplete multiple job transactions' do
      let(:external_id) { '34007' }
      let(:external_contact_id) { '44316' }
      let(:transaction) { xml_reader.transactions.select{|t| t[:external_id]==external_id}.first }
      let(:transacted_at) { DateTime.new(2014,9,9,12,0,0) }

      it 'should return the external_id, receipt_number and external_contact_id correctly' do
        expect(transaction[:external_id]).to eq(external_id)
        expect(transaction[:external_contact_id]).to eq(external_contact_id)
        expect(transaction[:receipt_number]).to eq(external_id)
      end

      it 'should return the maximum date of active jobs' do
        expect(transaction[:due_date]).to eq(transacted_at)
        expect(transaction[:transacted_at]).to eq(transacted_at)
      end

      it 'should return the total amount of active jobs' do
        expect(transaction[:amount]).to eq(11.0000)
      end

      it 'should return is_paid as true for completed projects' do
        expect(transaction[:is_paid]).to eq('0')
      end
    end

    context 'multiple job transactions w/o any of them completed' do
      let(:external_id) { '33983' }
      let(:external_contact_id) { '44293' }
      let(:transaction) { xml_reader.transactions.select{|t| t[:external_id]==external_id}.first }
      let(:transacted_at) { DateTime.new(2014,9,4,16,0,0) }

      it 'should return the external_id, receipt_number and external_contact_id correctly' do
        expect(transaction[:external_id]).to eq(external_id)
        expect(transaction[:external_contact_id]).to eq(external_contact_id)
        expect(transaction[:receipt_number]).to eq(external_id)
      end

      it 'should return the maximum date of active jobs' do
        expect(transaction[:due_date]).to eq(transacted_at)
        expect(transaction[:transacted_at]).to eq(transacted_at)
      end

      it 'should return the total amount of active jobs' do
        expect(transaction[:amount]).to eq(0.0000)
      end

      it 'should return is_paid as false for incomplete projects' do
        expect(transaction[:is_paid]).to eq('0')
      end
    end

    context 'A single customer with multiple projects' do
      let(:external_contact_id) { '99999' }
      let(:transactions) { xml_reader.transactions.select{|t| t[:external_contact_id]==external_contact_id}}

      it 'should return the correct project count for the customer' do
        expect(transactions.size).to eq(2)
      end

      it 'should return the correct totals for each individually' do
        expect(transactions.first[:amount]).to eq(101)
        expect(transactions[1][:amount]).to eq(202)
      end
    end
  end

  def get_jobs
    {
      :job1 => {
          'JobTotal' => 4
      },

      :job2 => {
          'JobTotal' => 1
      },

      :job3 => {},

      :job4 => {
          'JobTotal' => 2
      }
    }
  end
end
