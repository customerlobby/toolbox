module Toolbox
  class Client
    module Customers
      def customers(params={})
        ct = customers_transactions(params)
        ct.customers
      end
    end
  end
end
