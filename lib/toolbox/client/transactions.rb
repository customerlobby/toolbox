module Toolbox
  class Client
    module Transactions
      def transactions(params={})
        ct = customers_transactions(params)
        ct.transactions
      end
    end
  end
end
