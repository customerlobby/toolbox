module Toolbox
  class Client
    module CustomersTransactions
      def customers_transactions(params={})
        params[:accessKey] = self.api_key
        response = get('GetCustomerListByFranNum',params) || []
        xml_doc = Nokogiri::XML(response)
        xml_string = xml_doc.at_css('string').text
        rearranged_xml = "<string>#{xml_string}</string>"
        Toolbox::XmlReader.new(rearranged_xml)
      end
    end
  end
end
