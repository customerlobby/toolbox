module Toolbox
  # Wrapper for the Toolbox REST API.
  class Client < API
    Dir[File.expand_path('../client/*.rb', __FILE__)].each{|f| require f}

    include Toolbox::Client::CustomersTransactions
    include Toolbox::Client::Customers
    include Toolbox::Client::Transactions
  end
end
