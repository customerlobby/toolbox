require 'bigdecimal'
require 'nokogiri'

module Toolbox
  # XmlReader is used to encapsulate as much as possible of the resulting XML parsing
  # It utilizes a Nokogiri Nodeset to read the XML and convert to 2 arrays of hashes (customers, transactions)
  class XmlReader
    # @return [XmlReader] with the xml_string parsed into instance variables
    # @param xml_string [String] Xml String with the structure provided by the Toolbox API
    def initialize(xml_string)
      parse_xml(xml_string)
    end

    # @return [Array] of hashes as defined in parse_customers
    # This makes up half of the public interface, allowing access to a list of customers
    def customers
      @customers
    end

    # @return [Array] of hashes as defined in parse_transactions
    # This makes up half of the public interface, allowing access to a list of transactions
    def transactions
      @transactions
    end

    # @return [String] containing error message if any
    def errors
      @errors
    end

    private
    
    # @return nil
    # @param xml_string [String] Xml string from Toolbox API response.body
    def parse_xml(xml_string)
      xml_doc       = Nokogiri::XML(xml_string)
      @customers    = parse_customers(xml_doc)
      @transactions = parse_transactions(xml_doc)
      @errors        = parse_errors(xml_doc)
    end

    # @return [Array] of Hashes
    # @param xml_doc [Nokogiri::XML] Nokogiri XML nodeset
    # parses the needed areas of the XML nodeset and transforms that information into an array of hashes
    # each representing a customer.
    def parse_customers(xml_doc)
      return xml_doc.css('string Franchise Customer').map do |node|
        first_name, last_name, company_name = handle_name(node.css('SortName').text, node.at_css('Company').text)
        wants_communication                 = get_value(node, 'DoNotContact') == "0" ? "1" : "0"

        {
          external_id:         node['ID'],
          first_name:          first_name,
          last_name:           last_name,
          email:               get_value(node, 'EmailAddress'),
          phone_daytime:       get_value(node, 'Phone1'),
          phone_daytime_ext:   nil,
          phone_mobile:        get_value(node, 'Phone2'),
          address1:            get_value(node, 'Address1'),
          address2:            get_value(node, 'Address2'),
          city:                get_value(node, 'City'),
          state:               get_value(node, 'State'),
          zip:                 get_value(node, 'Zip'),
          country:             nil,
          wants_communication: wants_communication,
          commercial:          company_name.nil? ? '0' : '1',
          company_name:        company_name,
        }
      end
    end

    # @return [String]
    # @param node [Nokogiri::Node] Node to check the value of
    # @param field_name [String] field in the node we're interested in
    # Helper function that takes a node and field name, returning the value of the field or nil
    def get_value(node, field_name)
      value = node.at_css(field_name)
      return nil if value.nil? or value.text.empty?
      value.text
    end

    # @return [Array] of length 3, all values either nil or a strings
    # @param string_name [String] name of the person
    # @param company_name [String] name of the company
    # Determines whether the customer is a company or a person and then returns the name of that
    # entity.  If it's a customer, the format is "lastName, firstName" so we handle that via splitting.
    def handle_name(string_name, company_name)
      return [company_name, nil, company_name] unless (company_name.nil? || company_name == '')
      split = string_name.split(',')
      return [split[1].strip, split[0].strip, nil] if split.length >= 2
      [string_name, nil, nil]
    end

    # @return [Array] of hashes representing transactions
    # @param xml_doc [NodeSet] Nokogiri XML nodeset
    # Parses the transaction information from the NodeSet and organizes it into an array of hashes
    def parse_transactions(xml_doc)
      selected_nodes      = xml_doc.css('string Franchise Customer').select { |n| n.css('Project').count>0 }
      parsed_transactions = []

      # Toolbox seems to return either Status or JobStatus so we check for both
      status_fields = 'Job[JobStatus=Active], Job[JobStatus=Exported], Job[Status=Active], Job[Status=Exported]'

      selected_nodes.map do |node|
        project_nodes = node.css('Project')
        project_nodes.each do |project_node|

          project_id = project_node.attributes['Project_ID'].value
          jobs       = project_node.css(status_fields)
          next unless jobs.present?
          completed_count = get_completed_count(jobs)
          is_paid         = completed_count == jobs.size ? '1' : '0'

          max_date = get_max_date(jobs)
          amount   = get_amount_sum(jobs)

          parsed_transactions << {
            external_id:         project_id,
            external_contact_id: node['ID'],
            receipt_number:      project_id,
            due_date:            max_date,
            transacted_at:       max_date,
            amount:              amount,
            is_paid:             is_paid,
          }
        end
      end

      parsed_transactions
    end

    # @return [BigDecimal] a count of the # of completed jobs
    # @param jobs [NodeSet] of the jobs for this particular customer
    # Get the count of jobs that have a JobTotal value, indicating that it's completed.
    def get_completed_count(jobs)
      jobs.select { |j| !j['JobTotal'].blank? and BigDecimal.new(j['JobTotal']) }.count
    end

    # @return [DateTime] The latest date for TransmitDateTimeHO
    # @param jobs [NodeSet] of the jobs for this particular customer
    def get_max_date(jobs)

      dates = jobs.map { |j| j['ActualStartTime'] }
      dates = dates.select { |j| !j.nil? }
      return nil if dates.empty?
      max_date = dates.map { |j| DateTime.parse(j) }.sort.last

      return max_date
    end

    # @return [BigDecimal] the sum of the JobTotal values that's aren't empty
    # @param jobs [NodeSet] of the jobs for this particular customer
    def get_amount_sum(jobs)
      # jobs = jobs.select{ |j| !j['JobTotal'].nil? }
      jobs.select { |j| !j['JobTotal'].blank? }.map { |j| BigDecimal.new(j['JobTotal']) }.inject(0) { |sum, n| sum+n }
    end

    # @return [Array] an array of error messages if any errors are raised
    # @param xml_doc [NodeSet] Nokogiri XML nodeset
    def parse_errors(xml_doc)
      return xml_doc.css('string').map do |node|
        node.css('Error').text
      end
    end
  end
end
